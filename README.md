# Webdeploy

A modest program for deploying HTML, PHP, JavaScript, TypeScript and Node.JS apps for an Nginx webserver, or any combination of these tools.

**Table of Contents**

1. [About](#about)
2. [Features](#features)
3. [Usage](#usage)
4. [Configuration file](#configuration-file)
5. [Installation](#installation)
6. [Dependencies](#dependencies)
7. [Licence](#licence)

## About

This is a PHP program for building and deploying websites which use TypeScript, JavaScript, Node.JS and PHP languages and nginx as webserver.

_Webdeploy_ can be run in the root of a project directory. It reads the file 'webdeploy.yaml' for configuration.

## Features

**_Webdeploy_ is small, yet very capable! It can:**

- Be customized for even the smallest needs.
- Package a webroot directory for deployment, including:
  - Transpile TypeScript to JavaScript using [bun](https://bun.sh/) (the default), [swc](https://swc.rs/) or [tsc](https://www.typescriptlang.org/docs/handbook/compiler-options.html) (fallback);
  - Minify/uglify CSS and JavaScript through [esbuild](https://esbuild.github.io/).
  - Pre-compressing files using gzip to increase server performance a bit
- Securely transfer the webroot to specified target on a remote server or local host (using `SSH` and `rsync`).
- Deploy the Nginx configuration file in its `sites-available` folder and enable it by linking it to the `sites-enabled` folder.
- Disable the Nginx configuration by removing the link in `sites-enabled`.
- Deploy Node.JS apps and enable them using [PM2](https://pm2.io/).
- Disabling and wiping all installed components from the target host.
- Besides, it is very elegant and fast compared to larger alternatives.

**Upcoming features:**

- HTTPS certficicate request/renewal using [Certbot](https://certbot.eff.org/).

**Possible annoyances:**

- Its reliance on PHP means some dependencies will need to be installed before it can work.
- In order to use the transpiler and/or minifier, NPM and some modules will need to be installed.
- In order to deploy, the script currently requires root access on a target server.

## Usage

```
Usage:
  webdeploy < command > [ project_directory ]

Commands:
  l, list                 List configs and some info
  b, build [ CONFIG ]     Only build specified config
  e, enable [ CONFIG ]    Deploy and enable specified config
                          CONFIG is set to 'default' when omitted
  d, disable [ CONFIG ]   Disable specified config
  w, wipe [ CONFIG ]      Disable and completely wipe config from remote,
                          Warning: THIS WILL DELETE ALL DATA FROM SERVER
  (no command)            Show this help menu

Values:
  project_directory       Optionally specify the project directory,
                          defaults to current directory when omitted

Configuration file:
  The file 'webdeploy.yaml' is read and parsed from project_directory.
  This is where a CONFIG can be defined.
```

## Configuration file

The program reads 'webdeploy.yaml' in the project root as its configuration file. This file should contain a single _page_ with one or multiple named _configuration blocks_.

Here is a sample config with one configuration block called 'default':

```yaml
default: # 'default' | [custom name]
  remote_host: local # 'local' | [ssh hostname]
  webroot:
  - source: source # name of source directory
    output: build # [optional]: name of target build directory
    target: /var/www/html # name of target deploy directory on remote server
    owner: www-data # Unix username that the webserver runs as
    transpiler: bun # [optional]: 'bun' (default) | 'swc' | 'tsc'
    minify: false # [optional]: boolean
    gzip: true # [optional]: boolean, off by default
  nginx:
  - config: html-local.conf # name of nginx configuration file in project directory
  nodejs:
  - app: server.js # nodejs file on remote server
```

Explanation:

The above will transpile all TypeScript files from folder 'source' to 'build'. The 'build' folder gets completely overwritten, and all other files such as PHP, HTML, CSS and media get copied as well. Nothing is minified however (`minify: false`, which is useful for debugging). `gzip: true` is useful when Nginx' [gzip_static](https://docs.nginx.com/nginx/admin-guide/web-server/compression/) is enabled. This will pre-compile files and increase server performance, how much depends on the compressed file's size. _Webdeploy_ compresses files with the following extensions: `css, eot, html, js, otf, ttf, svg, xml`.

We selected server `remote_host: local`, which means the current system. This requires root privileges, so the program uses sudo where necessary.

The 'build' folder gets copied onto `/var/www/html` as specified, which is now owned by user `www-data` and has user permissions `rwX` (Read-Write for all files, eXecute for folders). Any files which already exist get overwritten, but nothing gets removed. This means old files will stay in place and will need to be removed manually.

Once the webroot deployment is done, the nginx config `html-local.conf` gets copied to `/etc/nginx/sites-available/` on the remote server. It then gets linked to `/etc/nginx/sites-enabled/` and checked using `nginx -qt`. When everything is in order, nginx is reloaded.

In `nodejs`, the specified `app: server.js` gets started using `pm2`.

### Extra Tricks (e.g. for Laravel)

#### deploy_source

The config file can now also contain a new option: `deploy_source`. This is useful when none of `source`, `output` or `target`  match the folder to be deployed. Consider the following example directories:

```
my-app
my-app/config
my-app/public
my-app/routes
my-app/source
```

If `my-app/source` needs to be built into `my-app/public`, but `my-app` needs to be deployed, the following can be used for example:

```yaml
#...
  webroot:
  - source: my-app/source
    build_output: my-app/public # 'build_output' is an alias for 'output'
    deploy_source: my-app
    deploy_target: /var/www/html # 'deploy_target' is an alias for 'target'
    #...
```

#### deploy_use_gitignore

As the name of this option implies, it will tell `rsync` (the command which deploys the files) to ignore files from `.gitignore`. Example snippet:

```yaml
#...
  webroot:
  - source: source
    deploy_target: /var/www/html
    deploy_use_gitignore: true
    #...
```

## Installation

This program is made for Linux systems, but will likely work on *BSD (and derivatives such as MacOS) too, albeit with some tweaking.

Before installing _Webdeploy_, make sure you have at least PHP and all _required_ dependencies installed ([see below](#dependencies)). Then copy 'webdeploy' to a folder which is in your [PATH](https://en.wikipedia.org/wiki/PATH_(variable)), such as `~/.local/bin/`, and add execute permissions.

Current user only, using simple installation script (recommended):

```sh
sh install.sh
```

Current user only, manual commands:

```sh
mkdir -p ~/.local/bin
cp webdeploy ~/.local/bin/
chmod 0700 ~/.local/bin/webdeploy
```

System-wide (requires root or sudo):

```sh
cp webdeploy /usr/local/bin/
chown root:root /usr/local/bin/webdeploy
chmod 0755 /usr/local/bin/webdeploy
```

## Dependencies

Software inside [] is optional.

Script: PHP with yaml module, bash (or other *nix shell)
Building: (npm): bun, swc, esbuild, typescript
Deploying: rsync, ssh client
On target server: rsync, ssh server, nginx webserver, [php-fpm, nodejs; (npm): pm2]

### APT (Debian and derivatives)

```sh
# On local machine
sudo apt install -y bash openssh-client rsync php php-yaml npm
sudo npm i -g typescript bun @swc/cli @swc/core esbuild

# On server
sudo apt install -y bash openssh-server rsync nginx php-fpm nodejs npm
sudo npm i -g pm2
```

### Pacman (Arch Linux and derivatives)

Since `php-yaml` is in the AUR, we will first install Git and base-devel. See [Makepkg - Arch Wiki](https://wiki.archlinux.org/title/Makepkg) for more info.

```sh
# On local machine
sudo pacman -Sy --needed git base-devel bash openssh rsync php npm
git clone https://aur.archlinux.org/php-yaml.git
cd php-yaml
makepkg -si
sudo npm i -g typescript bun @swc/cli @swc/core esbuild
# Optionally remove php-yaml folder afterwards:
cd ..
rm -rf php-yaml

# On server
sudo pacman -Sy bash openssh rsync nginx php-fpm nodejs npm
sudo npm i -g pm2
```

### DNF (Fedora and derivatives)

```sh
# On local machine
sudo dnf install -y bash bash openssh-clients rsync php php-yaml npm
sudo npm i -g typescript bun @swc/cli @swc/core esbuild

# On server
sudo dnf install -y bash openssh-server rsync nginx php-fpm nodejs npm
sudo npm i -g pm2
```

## Licence

SPDX Identifier: GPL-2.0-only

Copyright (C) 2023 Bart Nijbakker <bart@nijbakker.net>

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; version 2.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.