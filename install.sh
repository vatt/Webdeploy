#!/usr/bin/sh

# This script installs webdeploy to ~/.local/bin/
# It does not install dependencies, though!

# Install, exit on failure
if ! mkdir -p "$HOME/.local/bin" || ! cp webdeploy "$HOME/.local/bin/" || ! chmod 0700 "$HOME/.local/bin/webdeploy"; then
	echo "Failed to install. See above output."
	exit 1
fi

# Show info
echo "Webdeploy successfully installed to ~/.local/bin/webdeploy"
echo ""
echo "Note: Depencencies do not get installed automatically. Please install them manually, see README.md"

# Warn if script appears to be unavailable after install
if [ -z "$(which webdeploy)" ]; then
	echo "Warning: you may need to re-login, possibly because ~/.local/bin does not appear to be in your PATH."
	echo "You may run 'echo \$PATH' to confirm."
fi
